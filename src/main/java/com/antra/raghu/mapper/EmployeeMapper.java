package com.antra.raghu.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.antra.raghu.bo.EmployeeBO;
import com.antra.raghu.entity.EmployeeEntity;

@Component
public class EmployeeMapper {

	public EmployeeEntity mapToEntity(EmployeeBO bo) {
		EmployeeEntity entity = new EmployeeEntity();
		entity.setEmpId(bo.getEmployeeId());
		entity.setEmpName(bo.getEmployeeName());
		entity.setEmpSal(bo.getEmployeeSalary());
		entity.setEmpAddr(bo.getEmployeeAddress());
		return entity;
	}
	
	public EmployeeBO mapToBO(EmployeeEntity entity) {
		EmployeeBO bo = new EmployeeBO();
		bo.setEmployeeId(entity.getEmpId());
		bo.setEmployeeName(entity.getEmpName());
		bo.setEmployeeSalary(entity.getEmpSal());
		bo.setEmployeeAddress(entity.getEmpAddr());
		return bo;
	}
	
	public List<EmployeeEntity> mapToEntity(List<EmployeeBO> bos) {
		List<EmployeeEntity> entities = new ArrayList<>();
		for(EmployeeBO bo : bos) {
			entities.add(mapToEntity(bo));
		}
		return entities;
	}
	
	public List<EmployeeBO> mapToBO(List<EmployeeEntity> entities) {
		List<EmployeeBO> bos = new ArrayList<>();
		for(EmployeeEntity entity:entities ) {
			bos.add(mapToBO(entity));
		}
		return bos;
	}
}
