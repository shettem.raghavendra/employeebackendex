package com.antra.raghu.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.antra.raghu.bo.EmployeeBO;
import com.antra.raghu.service.IEmployeeService;

@RestController
@RequestMapping("/employee")
public class EmployeeRestController {
	
	@Autowired
	private IEmployeeService service;

	//1. add one employee
	@PostMapping("/create")
	public ResponseEntity<String> addEmployee(
			@RequestBody EmployeeBO employeeBO
			) 
	{
		Integer id = service.addEmployee(employeeBO);
		String message = new StringBuffer().append("Employee '")
				.append(id).append("' created").toString();
		return new ResponseEntity<>(message,HttpStatus.CREATED);
	}
	
	//2. view all employees
	@GetMapping("/all")
	public ResponseEntity<List<EmployeeBO>> getAllEmployees() {
		List<EmployeeBO> list = service.getAllEmployees();
		return new ResponseEntity<>(list,HttpStatus.OK);
	}
	
	//3. view one employee
	@GetMapping("/find/{id}")
	public ResponseEntity<EmployeeBO> getOneEmployee(
			@PathVariable Integer id
			) 
	{
		EmployeeBO bo = service.getOneEmployee(id);
		return new ResponseEntity<>(bo, HttpStatus.OK);
	}
	
	//4. delete one employee
	@DeleteMapping("/remove/{id}")
	public ResponseEntity<String> deleteEmployee(
			@PathVariable Integer id
			) 
	{
		service.deleteEmployee(id);
		String message = new StringBuffer().append("Employee '")
				.append(id).append("' deleted").toString();
		return new ResponseEntity<>(message, HttpStatus.OK);
	}
	
	//5. update one employee
	@PutMapping("/modify")
	public ResponseEntity<String> updateEmployee(
			@RequestBody EmployeeBO employeeBO
			) 
	{
		service.updateEmployee(employeeBO);
		String message = new StringBuffer().append("Employee '")
				.append(employeeBO.getEmployeeId())
				.append("' updated").toString();
		return new ResponseEntity<>(message, HttpStatus.OK);
	}
	
	
	
	
	
}
