package com.antra.raghu.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="employee_tab")
public class EmployeeEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="employee_id_col")
	private Integer empId;
	
	@Column(name="employee_name_col")
	private String empName;
	
	@Column(name="employee_sal_col")
	private Double empSal;
	
	@Column(name="employee_addr_col")
	private String empAddr;
	
}
