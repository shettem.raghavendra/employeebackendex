package com.antra.raghu.service;

import java.util.List;

import com.antra.raghu.bo.EmployeeBO;

public interface IEmployeeService {

	Integer addEmployee(EmployeeBO bo);
	List<EmployeeBO> getAllEmployees();
	EmployeeBO getOneEmployee(Integer id);
	void deleteEmployee(Integer id);
	void updateEmployee(EmployeeBO bo);
}
