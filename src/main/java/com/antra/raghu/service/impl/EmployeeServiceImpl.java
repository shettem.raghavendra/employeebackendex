package com.antra.raghu.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.antra.raghu.bo.EmployeeBO;
import com.antra.raghu.entity.EmployeeEntity;
import com.antra.raghu.exception.EmployeeNotFoundException;
import com.antra.raghu.mapper.EmployeeMapper;
import com.antra.raghu.repo.EmployeeRepository;
import com.antra.raghu.service.IEmployeeService;

@Service
public class EmployeeServiceImpl implements IEmployeeService {
	
	@Autowired
	private EmployeeRepository repo;
	@Autowired
	private EmployeeMapper mapper;

	@Override
	public Integer addEmployee(EmployeeBO bo) {
		EmployeeEntity entity = mapper.mapToEntity(bo);
		entity = repo.save(entity);
		return entity.getEmpId();
	}

	@Override
	public List<EmployeeBO> getAllEmployees() {
		List<EmployeeEntity> entities = repo.findAll();
		List<EmployeeBO> bos = mapper.mapToBO(entities);
		return bos;
	}

	@Override
	public EmployeeBO getOneEmployee(Integer id) {
		EmployeeBO bo = mapper.mapToBO(getEmployeeEntityById(id));
		return bo;
	}
	
	private EmployeeEntity getEmployeeEntityById(Integer id) {
		return repo.findById(id)
				.orElseThrow(
						()->new EmployeeNotFoundException(
								new StringBuffer().append("Employee '")
								.append(id).append("' Not Exist").toString()
								)
						);
	}

	@Override
	public void deleteEmployee(Integer id) {
		EmployeeEntity entity = getEmployeeEntityById(id);
		repo.delete(entity); 
	}

	@Override
	public void updateEmployee(EmployeeBO bo) {
		EmployeeEntity entity = getEmployeeEntityById(bo.getEmployeeId());
		if(StringUtils.hasText(bo.getEmployeeName()))
			entity.setEmpName(bo.getEmployeeName());
		if(bo.getEmployeeSalary() != 0)
			entity.setEmpSal(bo.getEmployeeSalary());
		if(StringUtils.hasText(bo.getEmployeeAddress()))
			entity.setEmpAddr(bo.getEmployeeAddress());
		repo.save(entity);
	}

}
