package com.antra.raghu.bo;

import lombok.Data;

@Data
public class EmployeeBO {

	private Integer employeeId;
	private String employeeName;
	private Double employeeSalary;
	private String employeeAddress;
	
}
