package com.antra.raghu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeeBackendExApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeeBackendExApplication.class, args);
	}

}
